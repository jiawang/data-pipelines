--
-- Creates a table for uploads statistics of Commons on a monthly basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_commons_uploads_monthly.hql \
--         -d table_name=wmf_product.commons_uploads_monthly \
--         -d base_directory=/wmf/data/wmf_product/structured_content/commons_uploads_monthly


CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
    `snapshot_date`                               date        COMMENT "YYYY-MM-01",
    `all_uploaded_count`                          bigint      COMMENT "Total number of uploads",
    `all_deleted_count`                           bigint      COMMENT "Total number of uploads that have been deleted",
    `uploadwizard_uploaded_count`                 bigint      COMMENT "Number of uploads using UploadWizard",
    `uploadwizard_deleted_count`                  bigint      COMMENT "Number of uploads using UploadWizard that have been deleted",
    `uploadwizard_uploaded_own_work_count`        bigint      COMMENT "Number of uploads using UploadWizard that are own work",
    `uploadwizard_deleted_own_work_count`         bigint      COMMENT "Number of uploads using UploadWizard that are own work that has been deleted",
    `uploadwizard_uploaded_not_own_work_count`    bigint      COMMENT "Number of uploads using UploadWizard that are not own work",
    `uploadwizard_deleted_not_own_work_count`     bigint      COMMENT "Number of uploads using UploadWizard that are not own work that has been deleted"
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
