--
-- Populates the commons_uploads table monthly,
-- filtering edit and logging data and aggregating it into interesting dimensions.
--
-- Parameters:
--     source_history_table           -- Fully qualified table name of the source mediawiki history data
--     source_logging_table           -- Fully qualified table name of the source mediawiki logging data
--     source_private_comment_table   -- Fully qualified table name of the source mediawiki private comment data
--     destination_table              -- Fully qualified table name for Commons UploadWizard uploads data
--     snapshot                       -- Snapshot of the partition to process
--     coalesce_partitions            -- Number of partitions to write

--
-- Usage:
--     spark3-sql -f generate_commons_uploads_monthly.hql \
--         -d source_history_table=wmf.mediawiki_history \
--         -d source_logging_table=wmf_raw.mediawiki_logging \
--         -d source_private_comment_table=wmf_raw.mediawiki_private_comment \
--         -d destination_table=wmf_product.commons_uploads_monthly \
--         -d snapshot=2024-07 \
--         -d coalesce_partitions=1


-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    snapshot_date >= TO_DATE('${snapshot}') - INTERVAL 1 MONTH
    AND snapshot_date < TO_DATE('${snapshot}')
;

WITH ud_uploads AS (
    SELECT
        TO_DATE(TRUNC(event_timestamp,'MONTH')) AS snapshot_date,
        page_id,
        CASE WHEN ARRAY_CONTAINS(revision_tags, 'uploadwizard') THEN true ELSE false END AS upload_wizard,
        CASE WHEN LOWER(event_comment) LIKE '%own work%' THEN true ELSE false END AS own_work,
        page_is_deleted
    FROM ${source_history_table}
    WHERE snapshot = '${snapshot}'
    AND event_timestamp >= TO_TIMESTAMP('${snapshot}') - INTERVAL 1 MONTH
    AND event_timestamp < TO_TIMESTAMP('${snapshot}')
    AND wiki_db = 'commonswiki'
    AND event_entity = 'revision'
    AND event_type = 'create'
    AND revision_parent_id = 0
    AND page_namespace = 6
    AND NOT page_is_redirect
    AND NOT page_is_deleted
),
d_uploads AS (
    SELECT
        TO_DATE(TRUNC(event_timestamp,'MONTH')) AS snapshot_date,
        page_id,
        CASE WHEN ARRAY_CONTAINS(revision_tags, 'uploadwizard') THEN true ELSE false END AS upload_wizard,
        CASE WHEN LOWER(c.comment_text) LIKE '%own work%' THEN true ELSE false END AS own_work,
        page_is_deleted
    FROM ${source_history_table} mh
        LEFT JOIN ${source_logging_table} l ON mh.page_id = l.log_page
        LEFT JOIN ${source_private_comment_table} c  ON l.log_comment_id = c.comment_id
    WHERE mh.snapshot = '${snapshot}'
        AND l.snapshot = '${snapshot}'
        AND c.snapshot = '${snapshot}'
        AND event_timestamp >= TO_TIMESTAMP('${snapshot}') - INTERVAL 1 MONTH
        AND event_timestamp < TO_TIMESTAMP('${snapshot}')
        AND mh.wiki_db = 'commonswiki'
        AND c.wiki_db = 'commonswiki'
        AND l.wiki_db = 'commonswiki'
        AND event_entity = 'revision'
        AND event_type = 'create'
        AND revision_parent_id = 0
        AND page_namespace = 6
        AND log_type = 'upload'
        AND log_action = 'upload'
        AND log_namespace = 6
        AND page_is_deleted
)

INSERT INTO TABLE ${destination_table}
SELECT  /*+ COALESCE(${coalesce_partitions}) */
    snapshot_date,
    COUNT(DISTINCT page_id) AS all_uploaded_count,
    COUNT(DISTINCT(CASE WHEN page_is_deleted THEN page_id END)) AS all_deleted_count,
    COUNT(DISTINCT(CASE WHEN upload_wizard THEN page_id END)) AS uploadwizard_uploaded_count,
    COUNT(DISTINCT(CASE WHEN upload_wizard AND page_is_deleted THEN page_id END)) AS uploadwizard_deleted_count,
    COUNT(DISTINCT(CASE WHEN upload_wizard AND own_work THEN page_id END)) AS uploadwizard_uploaded_own_work_count,
    COUNT(DISTINCT(CASE WHEN upload_wizard AND own_work AND page_is_deleted THEN page_id END)) AS uploadwizard_deleted_own_work_count,
    COUNT(DISTINCT(CASE WHEN upload_wizard AND NOT own_work THEN page_id END)) AS uploadwizard_uploaded_not_own_work_count,
    COUNT(DISTINCT(CASE WHEN upload_wizard AND NOT own_work AND page_is_deleted THEN page_id END)) AS uploadwizard_deleted_not_own_work_count
FROM (
    SELECT * FROM ud_uploads
        UNION
    SELECT * FROM d_uploads
    )
GROUP BY snapshot_date
;

