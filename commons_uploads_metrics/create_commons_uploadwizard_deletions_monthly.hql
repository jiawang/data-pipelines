--
-- Creates a table for Upload Wizard deletions statistics of Commons on a monthly basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_commons_uploadwizard_deletions_monthly.hql \
--         -d table_name=wmf_product.commons_uploadwizard_deletions_monthly \
--         -d base_directory=/wmf/data/wmf_product/structured_content/commons_uploadwizard_deletions_monthly

CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
    `snapshot_date`                                             date        COMMENT "YYYY-MM-01",
    `with_deletion_request_count`                               bigint      COMMENT "Number of uploads using upload wizard that have been deleted with deletion requests",
    `without_deletion_request_count`                            bigint      COMMENT "Number of uploads using upload wizard that have been deleted without deletion requests",
    `with_deletion_request_within30d_count`                     bigint      COMMENT "Number of uploads using upload wizard that have been deleted with deletion requests within 30 days",
    `without_deletion_request_within30d_count`                  bigint      COMMENT "Number of uploads using upload wizard that have been deleted without deletion requests within 30 days",
    `with_deletion_request_copyright_only_within30d_count`      bigint      COMMENT "Number of uploads using upload wizard that have been deleted because of copyright violation with deletion requests within 30 days",
    `without_deletion_request_copyright_only_within30d_count`   bigint      COMMENT "Number of uploads using upload wizard that have been deleted because of copyright violation without deletion requests within 30 days",
    `with_deletion_request_fop_only_within30d_count`            bigint      COMMENT "Number of uploads using upload wizard that have been deleted because of freedom of panorama violation with deletion requests within 30 days",
    `without_deletion_request_fop_only_within30d_count`         bigint      COMMENT "Number of uploads using upload wizard that have been deleted because of freedom of panorama violation without deletion requests within 30 days"
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;