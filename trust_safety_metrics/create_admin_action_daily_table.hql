--
-- Creates a table for administration action statistics on a daily basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_admin_action_daily_table.hql \
--         -d table_name=wmf_product.trust_safety_admin_action_daily \
--         -d base_directory=/wmf/data/wmf_product/trust_safety_metrics/admin_action_daily


CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
    `date_time`         timestamp   COMMENT "YYYY-MM-DD 00:00:00.000",
    `database_group`    string      COMMENT "Project family, e.g. wikipedia",
    `wiki_db`           string      COMMENT "Wiki ID, e.g. enwiki",
    `deleted_pages`     bigint      COMMENT "Number of pages which were deleted",
    `reverted_edits`    bigint      COMMENT "Number of edits which were reverted",
    `protected_pages`   bigint      COMMENT "Number of protected pages",
    `edit_rollbacks`    bigint      COMMENT "Number of rollbacks which reverted any existing edits"
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
