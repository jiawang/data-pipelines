--
-- Creates a table for block statistics on a daily basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_block_daily_table.hql \
--         -d table_name=wmf_product.trust_safety_block_daily \
--         -d base_directory=/wmf/data/wmf_product/trust_safety_metrics/block_daily


CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
    `date_time`             timestamp   COMMENT "YYYY-MM-DD 00:00:00.000",
    `database_group`        string      COMMENT "Project family, e.g. wikipedia",
    `wiki_db`               string      COMMENT "Wiki ID, e.g. enwiki",
    `log_action`            string      COMMENT "Type of actions, e.g. block, reblock or unblock",
    `block_type`            string      COMMENT "Type of blocks, e.g. sidewide (block) or partial (block)",
    `ipb_user_type`         string      COMMENT "Type of users or IPs that are blocked, e.g. register_user, temp_user, ip_single or ip_range",
    `blocks`                bigint      COMMENT "Number of block events",
    `user_or_ip_blocked`    bigint      COMMENT "Number of unique blocked users/ips"
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;

