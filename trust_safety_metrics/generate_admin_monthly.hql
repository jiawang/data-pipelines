--
-- Populates the mediawiki_history table and the mediawiki_logging table monthly,
-- filtering administrator and edit data, then aggregating them into interesting dimensions.
--
-- Parameters:
--     source_history_table         -- Fully qualified table name of the source mediawiki history data
--     source_logging_table         -- Fully qualified table name of the source mediawiki logging data
--     destination_table            -- Fully qualified table name for the administrator data
--     canonical_table              -- Fully qualified table name for canonical wiki data
--     snapshot                     -- Snapshot of the partition to process
--     coalesce_partitions          -- Number of partitions to write

--
-- Usage:
--     spark3-sql -f generate_admin_monthly_table.hql \
--         -d source_history_table=wmf.mediawiki_history \
--         -d source_logging_table=wmf_raw.mediawiki_logging \
--         -d destination_table=wmf_product.trust_safety_admin_monthly \
--         -d canonical_table=canonical_data.wikis \
--         -d snapshot=2024-04 \
--         -d coalesce_partitions=1


-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    month_time >= TO_TIMESTAMP('${snapshot}')
    AND month_time < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
;


WITH t_a AS (
    SELECT  TRUNC(TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss'), 'MONTH')AS month_time,
        database_group,
        wiki_db,
        COUNT(DISTINCT log_actor) AS admins
    FROM ${source_logging_table}
    INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code AND
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
    WHERE snapshot ='${snapshot}' AND log_type IN ('block', 'protect', 'delete', 'rights')
    AND log_action NOT IN ('delete_redir', 'move_prot', 'autopromote')
    AND TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss')  >= TO_TIMESTAMP('${snapshot}')
    AND TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss') < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
    GROUP BY   TRUNC(TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss'), 'MONTH'), database_group, wiki_db
),
t_e AS (
    SELECT TRUNC(event_timestamp,'MONTH') AS month_time,
        database_group,wiki_db,
        COUNT(revision_id) AS edits
    FROM ${source_history_table}
    INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code and
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
    WHERE
            snapshot = '${snapshot}'
            AND event_entity = 'revision'
            AND event_type = 'create'
            AND event_timestamp >= TO_TIMESTAMP('${snapshot}')
            AND event_timestamp < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
    GROUP BY TRUNC(event_timestamp,'MONTH'),database_group,wiki_db
)

INSERT INTO TABLE ${destination_table}
SELECT  /*+ COALESCE(${coalesce_partitions}) */
    t_a.month_time,
    t_a.database_group,
    t_a.wiki_db,
    t_a.admins,
    t_e.edits,
    round(COALESCE(t_e.edits, 0L)/t_a.admins,0) AS edits_admin_ratio
FROM t_a
LEFT JOIN t_e ON t_a.month_time = t_e.month_time AND t_a.database_group=t_e.database_group AND t_a.wiki_db = t_e.wiki_db
;
