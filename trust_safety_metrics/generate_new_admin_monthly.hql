--
-- Populates the mediawiki_history table and the mediawiki_logging table monthly,
-- filtering administrator and edit data, then aggregating them into interesting dimensions.
--
-- Parameters:
--     source_logging_table         -- Fully qualified table name of the source mediawiki logging data
--     destination_table            -- Fully qualified table name for the new administrator data
--     canonical_table              -- Fully qualified table name for canonical wiki data
--     snapshot                     -- Snapshot of the partition to process
--     coalesce_partitions          -- Number of partitions to write

--
-- Usage:
--     spark3-sql -f generate_new_admin_monthly_table.hql \
--         -d source_logging_table=wmf_raw.mediawiki_logging \
--         -d destination_table=wmf_product.trust_safety_new_admin_monthly \
--         -d canonical_table=canonical_data.wikis \
--         -d snapshot=2024-04 \
--         -d coalesce_partitions=1
--

-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    month_time >= TO_TIMESTAMP('${snapshot}')
    AND month_time < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
;

WITH t_1st AS (
    SELECT wiki_db,
        log_actor,
        MIN(TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss' )) AS first_admin_event
    FROM ${source_logging_table}
    WHERE snapshot ='${snapshot}' AND log_type IN ('block', 'protect', 'delete', 'rights')
    AND log_action NOT IN ('delete_redir', 'move_prot', 'autopromote')
    GROUP BY wiki_db, log_actor
    HAVING first_admin_event >= TO_TIMESTAMP('${snapshot}')
    AND first_admin_event < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
)

INSERT INTO TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
    TRUNC(t_1st.first_admin_event, 'MONTH') AS month_time,
    database_group,
    wiki_db,
    COUNT(DISTINCT t_1st.log_actor) AS new_admins
FROM t_1st
INNER JOIN ${canonical_table}
ON
    t_1st.wiki_db = database_code AND
    database_group IN (
        'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
        'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
        'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
    )
GROUP BY TRUNC(t_1st.first_admin_event, 'MONTH'), database_group, wiki_db
;
