--
-- Creates a table for administrator statistics on a monthly basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_admin_monthly_table.hql \
--         -d table_name=wmf_product.trust_safety_admin_monthly \
--         -d base_directory=/wmf/data/wmf_product/trust_safety_metrics/admin_monthly


CREATE EXTERNAL TABLE IF NOT EXISTS ${table_name}(
    `month_time`        timestamp   COMMENT "YYYY-MM-01 00:00:00.000",
    `database_group`    string      COMMENT "Project family, e.g. wikipedia",
    `wiki_db`           string      COMMENT "Wiki ID, e.g. enwiki",
    `admins`            bigint      COMMENT "Number of active adminstrators",
    `edits`             bigint      COMMENT "Number of edits",
    `edits_admin_ratio` bigint      COMMENT "Edits admin ratio"
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
