--
-- Populates the mediawiki_history table and the mediawiki_logging table monthly,
-- filtering delete, revert and protect action data and aggregating it into interesting dimensions.
--
-- Parameters:
--     source_history_table         -- Fully qualified table name of the source mediawiki history data
--     source_logging_table         -- Fully qualified table name of the source mediawiki logging data
--     destination_table            -- Fully qualified table name for the administration action data
--     canonical_table              -- Fully qualified table name for canonical wiki data
--     snapshot                     -- Snapshot of the partition to process
--     coalesce_partitions          -- Number of partitions to write

--
-- Usage:
--     spark3-sql -f generate_admin_action_monthly_table.hql \
--         -d source_history_table=wmf.mediawiki_history \
--         -d source_logging_table=wmf_raw.mediawiki_logging \
--         -d destination_table=wmf_product.trust_safety_admin_action_monthly \
--         -d canonical_table=canonical_data.wikis \
--         -d snapshot=2024-04 \
--         -d coalesce_partitions=1

-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    month_time>= TO_TIMESTAMP('${snapshot}')
    AND month_time < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
;

WITH t_delete AS (
    SELECT TRUNC(event_timestamp,'MONTH') AS month_time,
        database_group,
        wiki_db,
        COUNT(1) AS deleted_pages
    FROM ${source_history_table}
    INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code AND
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
    WHERE snapshot= '${snapshot}'
    AND event_timestamp >= TO_TIMESTAMP('${snapshot}')
    AND event_timestamp < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
    AND event_type = 'delete'
    GROUP BY TRUNC(event_timestamp,'MONTH'), database_group, wiki_db
),
t_revert  AS (
    SELECT TRUNC(event_timestamp,'MONTH') AS month_time,
        database_group,
        wiki_db,
        COUNT(1) AS reverted_edits
    FROM ${source_history_table}
    INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code AND
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
    WHERE snapshot = '${snapshot}'
    AND event_timestamp >= TO_TIMESTAMP('${snapshot}')
    AND event_timestamp < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
    AND event_entity = 'revision' AND revision_is_identity_reverted
    GROUP BY TRUNC(event_timestamp,'MONTH'), database_group, wiki_db
),
t_rollback AS (
    SELECT TRUNC(event_timestamp,'MONTH') AS month_time,
        database_group,
        wiki_db,
        COUNT(1) AS edit_rollbacks
    FROM ${source_history_table}
    INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code and
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
    WHERE snapshot = '${snapshot}'
    AND event_timestamp >= TO_TIMESTAMP('${snapshot}')
    AND event_timestamp < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
    AND event_entity = 'revision' AND revision_is_identity_revert
    GROUP BY TRUNC(event_timestamp,'MONTH'), database_group, wiki_db
),
t_protect AS (
    SELECT TRUNC(TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss'), 'MONTH') AS month_time,
        database_group,
        wiki_db,
        COUNT(DISTINCT log_page) AS protected_pages
    FROM ${source_logging_table}
    INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code AND
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
    WHERE snapshot = '${snapshot}'
    AND TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss')  >= TO_TIMESTAMP('${snapshot}')
    AND TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss') < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
    AND log_action = 'protect'
    GROUP BY TRUNC(TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss'), 'MONTH'), database_group, wiki_db
)

INSERT INTO TABLE ${destination_table}
SELECT  /*+ COALESCE(${coalesce_partitions}) */
    COALESCE(
        t_delete.month_time,
        t_revert.month_time,
        t_protect.month_time,
        t_rollback.month_time
    ) AS month_time,
    COALESCE(
        t_delete.database_group,
        t_revert.database_group,
        t_protect.database_group,
        t_rollback.database_group
    ) AS database_group,
    COALESCE(
        t_delete.wiki_db,
        t_revert.wiki_db,
        t_protect.wiki_db,
        t_rollback.wiki_db
    ) AS wiki_db,
    CASE WHEN ISNULL(t_delete.deleted_pages) THEN 0 ELSE t_delete.deleted_pages END AS deleted_pages,
    CASE WHEN ISNULL(t_revert.reverted_edits) THEN 0 ELSE t_revert.reverted_edits END AS reverted_edits,
    CASE WHEN ISNULL(t_protect.protected_pages) THEN 0 ELSE t_protect.protected_pages END AS protected_pages,
    CASE WHEN ISNULL(t_rollback.edit_rollbacks) THEN 0 ELSE t_rollback.edit_rollbacks END AS edit_rollbacks
FROM t_delete FULL OUTER JOIN t_revert
ON t_delete.month_time = t_revert.month_time AND t_delete.database_group = t_revert.database_group
AND t_delete.wiki_db = t_revert.wiki_db
FULL OUTER JOIN t_protect
ON t_protect.month_time = t_revert.month_time AND t_protect.database_group = t_revert.database_group
AND t_protect.wiki_db = t_revert.wiki_db
FULL OUTER JOIN t_rollback
ON t_rollback.month_time = t_revert.month_time AND t_rollback.database_group = t_revert.database_group
AND t_rollback.wiki_db = t_revert.wiki_db
;
