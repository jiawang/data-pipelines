--
-- Populates the mediawiki_logging table monthly,
-- filtering block data and aggregating it into interesting dimensions.
--
-- Parameters:
--     source_table         -- Fully qualified table name of the source logging data
--     destination_table    -- Fully qualified table name for the wikipedia block data
--     canonical_table      -- Fully qualified table name for canonical wiki data
--     snapshot             -- Snapshot of the partition to process
--     coalesce_partitions          -- Number of partitions to write

--
-- Usage:
--     spark3-sql -f generate_block_daily.hql \
--         -d source_table=wmf_raw.mediawiki_logging \
--         -d destination_table=wmf_product.trust_safety_block_daily \
--         -d canonical_table=canonical_data.wikis \
--         -d snapshot=2024-04 \
--         -d coalesce_partitions=1


-- Delete existing data for the period to prevent duplication of data in case of recomputation
DELETE FROM ${destination_table}
WHERE
    date_time >= TO_TIMESTAMP('${snapshot}')
    AND date_time < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
;

WITH t_b AS (
    SELECT
        TO_DATE(TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss'))  AS date_time,
        wiki_db,
        database_group,
        log_action,
        log_title,
        -- If sitewide is 0, it's a partial block. If log_params is empty, it's a sitewide block.
        CASE
            WHEN log_params regexp '\"sitewide\"\;b:0' THEN 'partial'
            ELSE 'sitewide'
        END AS block_type,
        CASE
            WHEN log_title regexp '.*\/.*' THEN 'ip_range'
            WHEN log_title regexp '.*[\.:].*' THEN 'ip_single'
            WHEN log_title regexp  '^[~]2' THEN 'temp_user'
            ELSE 'register_user'
        END AS ipb_user_type
    FROM ${source_table}
    INNER JOIN ${canonical_table}
    ON
        wiki_db = database_code AND
        database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki', 'meta', 'sources',
            'species','wikibooks', 'wikidata', 'wikinews', 'wikipedia', 'wikiquote',
            'wikisource', 'wikiversity', 'wikivoyage', 'wiktionary'
        )
    WHERE snapshot = '${snapshot}' AND log_type = 'block'
    AND log_action IN ('block','reblock','unblock')
    AND TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss')  >= TO_TIMESTAMP('${snapshot}')
    AND TO_TIMESTAMP(log_timestamp,'yyyyMMddHHmmss') < TO_TIMESTAMP('${snapshot}') + INTERVAL 1 MONTH
)

INSERT INTO TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
    t_b.date_time,
    t_b.database_group,
    t_b.wiki_db,
    t_b.log_action,
    t_b.block_type,
    t_b.ipb_user_type,
    COUNT(1) AS blocks,
    COUNT(DISTINCT t_b.log_title) AS user_or_ip_blocked
FROM t_b
GROUP BY t_b.date_time, t_b.database_group, t_b.wiki_db,t_b.log_action, t_b.block_type,t_b.ipb_user_type
;
