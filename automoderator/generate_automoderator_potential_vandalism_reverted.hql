--
-- Generates monthly snapshot of potential vandalism reverted by Automoderator.
--
-- Parameters:
--     automod_config_table         -- Fully qualified table name containing Automoderator's latest configuration.
--     automod_small_wikis_table    -- Fully qualified table name containing small wikis classification for Automoderator.
--     source_mwh_table             -- Fully qualified table name of mediawiki_history.
--     destination_table            -- Fully qualified table name destination table.
--     snapshot                     -- Snapshot of the partition to process.
--     coalesce_partitions          -- Number of partitions to write.
--
--
-- Usage:
--     spark3-sql -f generate_automoderator_potential_vandalism_reverted.hql \
--         -d automod_config_table=wmf_product.automoderator_config \
--         -d automod_small_wikis_table=wmf_product.automoderator_small_wikis_classification \
--         -d source_mwh_table=wmf.mediawiki_history \
--         -d destination_table=wmf_product.automoderator_potential_vandalism_reverted \
--         -d snapshot=2024-09 \
--         -d coalesce_partitions=1

WITH config AS (
    SELECT
        config.wiki_db,
        automoderator_user_name,
        COALESCE(is_small_wiki, FALSE) AS is_small_wiki
    FROM
        ${automod_config_table} AS config
    LEFT JOIN
        ${automod_small_wikis_table} AS sw
        ON config.wiki_db = sw.wiki_db
    WHERE
        config.snapshot_date = (
            SELECT MAX(snapshot_date)
            FROM ${automod_config_table}
        )
),

-- criteria for potential vandalism (within Automoderator's scope)
-- https://phabricator.wikimedia.org/T349083#9410722
potential_vandal_revs AS (
    SELECT
        pvandal.wiki_db,
        is_small_wiki,
        DATE(event_timestamp) AS revision_date,
        revision_id,
        event_user_text AS user_name,
        CASE
            WHEN event_user_is_anonymous THEN 'anonymous'
            WHEN (NOT event_user_is_anonymous) AND (event_user_text LIKE '~2%') THEN 'temporary'
            WHEN NOT (event_user_is_anonymous OR event_user_text LIKE '~2%') AND event_user_revision_count < 50 THEN 'newcomer'
            ELSE 'other_registered'
        END AS user_type,
        revision_first_identity_reverting_revision_id
    FROM
        ${source_mwh_table} AS pvandal
    INNER JOIN
        config
        ON pvandal.wiki_db = config.wiki_db
    WHERE
        snapshot = '${snapshot}'
        AND event_entity = 'revision'
        AND event_type = 'create'
        AND page_namespace_is_content
        AND (
            event_user_is_anonymous
            OR event_user_revision_count <= 15
        )
        AND SIZE(event_user_is_bot_by_historical) = 0
        AND revision_is_identity_reverted
        AND revision_seconds_to_identity_revert <= 12*60*60
        AND revision_seconds_to_identity_revert > 0
        -- date of first deployment of Automoderator
        AND DATE(event_timestamp) > DATE('2024-05-25') 
),

reverts AS (
    SELECT
        pvr.*,
        CASE
            WHEN event_user_text = automoderator_user_name THEN 'automoderator'
            WHEN SIZE(event_user_is_bot_by_historical) > 0 THEN 'bot'
            WHEN event_user_is_anonymous THEN 'anonymous'
            WHEN (NOT event_user_is_anonymous) AND (event_user_text LIKE '~2%') THEN 'temporary'
            ELSE 'registered_users'
        END AS revert_performer_user_type
    FROM
        potential_vandal_revs pvr
    INNER JOIN
        ${source_mwh_table} AS reverts_mwh
        ON pvr.wiki_db = reverts_mwh.wiki_db
            AND pvr.revision_first_identity_reverting_revision_id = reverts_mwh.revision_id
    LEFT JOIN
        config
        ON pvr.wiki_db = config.wiki_db
    WHERE
        reverts_mwh.snapshot = '${snapshot}'
        AND reverts_mwh.event_entity = 'revision'
        AND reverts_mwh.event_type = 'create'
        AND reverts_mwh.page_namespace_is_content
        AND reverts_mwh.revision_is_identity_revert
        -- exclude self-reverts
        AND NOT pvr.user_name = reverts_mwh.event_user_text
)

INSERT OVERWRITE TABLE ${destination_table}
PARTITION (snapshot = '${snapshot}')
SELECT /*+ COALESCE(${coalesce_partitions}) */
    wiki_db,
    is_small_wiki,
    CAST(revision_date AS DATE) AS revision_date,
    revert_performer_user_type,
    COUNT(DISTINCT revision_id) AS reverted_revision_count
FROM
    reverts
GROUP BY
    wiki_db,
    is_small_wiki,
    revision_date,
    revert_performer_user_type
;