--
-- Generates montly snapshot of Automoderator's activity.
--
-- Parameters:
--     automod_config_table         -- Fully qualified table name containing Automoderator's latest configuration.
--     automod_small_wikis_table    -- Fully qualified table name containing small wikis classification for Automoderator.
--     source_mwh_table             -- Fully qualified table name of mediawiki_history.
--     destination_table            -- Fully qualified table name destination table.
--     snapshot                     -- Snapshot of the partition to process.
--     coalesce_partitions          -- Number of partitions to write.

--
-- Usage:
--     spark3-sql -f generate_automoderator_activity_snapshot_monthly.hql \
--         -d automod_config_table=wmf_product.automoderator_config \
--         -d automod_small_wikis_table=wmf_product.automoderator_small_wikis_classification \
--         -d source_mwh_table=wmf.mediawiki_history \
--         -d destination_table=wmf_product.automoderator_activity_snapshot_monthly \
--         -d snapshot=2024-08 \
--         -d coalesce_partitions=1

WITH latest_config AS (
    SELECT
        config.wiki_db,
        automoderator_user_name,
        COALESCE(is_small_wiki, FALSE) AS is_small_wiki
    FROM
        ${automod_config_table} AS config
    LEFT JOIN
        ${automod_small_wikis_table} AS sw
        ON config.wiki_db = sw.wiki_db
    WHERE
        config.snapshot_date = (
            SELECT MAX(snapshot_date)
            FROM ${automod_config_table}
        )
),

-- =======================================================================
-- data related to reverts made by Automoderator (prefix: amr_)
-- =======================================================================
automod_reverts AS (
    SELECT
        mwh.snapshot AS snapshot,
        mwh.wiki_db,
        is_small_wiki,
        revision_id AS amr_rev_id,
        page_namespace AS amr_page_namespace,
        CAST(event_timestamp AS TIMESTAMP) AS amr_rev_dt,
        DATE(event_timestamp) AS amr_rev_date,
        revision_is_identity_revert AS is_amr_revert,
        revision_is_identity_reverted AS is_amr_reverted,
        revision_parent_id AS amr_rev_parent_id,
        revision_first_identity_reverting_revision_id AS amr_revam_rev_id, 
        revision_seconds_to_identity_revert AS revam_ttr_sec
    FROM
        ${source_mwh_table} mwh
    INNER JOIN
        latest_config config
        ON mwh.wiki_db = config.wiki_db
           AND mwh.event_user_text = config.automoderator_user_name
    WHERE
        snapshot = '${snapshot}'
        AND event_entity = 'revision'
        AND event_type = 'create'
),

-- =========================================================================
-- data related to edits that were reverted by Automoderator (prefix: revr_)
-- =========================================================================
reverted_edits AS (
    SELECT
        amr.*,
        revr_mwh.page_namespace AS revr_page_namespace,
        revr_mwh.revision_id AS revr_rev_id,
        revr_mwh.event_user_text AS revr_actor_name,
        CAST(revr_mwh.event_timestamp AS TIMESTAMP) AS revr_rev_dt,
        DATE(revr_mwh.event_timestamp) AS revr_rev_date,
        CASE
            WHEN revr_mwh.event_user_is_anonymous THEN 'anonymous'
            WHEN (NOT revr_mwh.event_user_is_anonymous) AND (revr_mwh.event_user_text LIKE '~2%') THEN 'temporary'
            WHEN NOT (revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%') AND revr_mwh.event_user_revision_count < 50 THEN 'newcomer'
            ELSE 'other_registered'
        END AS revr_user_type,
        CASE
            WHEN revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%' THEN NULL
            WHEN NOT (revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%') AND revr_mwh.event_user_revision_count = 0 THEN '0'
            WHEN NOT (revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%') AND revr_mwh.event_user_revision_count BETWEEN 1 AND 5 THEN '1-5'
            WHEN NOT (revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%') AND revr_mwh.event_user_revision_count BETWEEN 6 AND 99 THEN '6-99'
            WHEN NOT (revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%') AND revr_mwh.event_user_revision_count BETWEEN 100 AND 999 THEN '100-999'
            WHEN NOT (revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%') AND revr_mwh.event_user_revision_count BETWEEN 1000 AND 4999 THEN '1000-4999'
            WHEN NOT (revr_mwh.event_user_is_anonymous OR revr_mwh.event_user_text LIKE '~2%') AND revr_mwh.event_user_revision_count >= 5000 THEN '5000+'
        END AS revr_user_editcount_bucket,
        revr_mwh.revision_seconds_to_identity_revert AS amr_ttr_sec,
        CASE
            WHEN ARRAY_CONTAINS(revision_tags, 'mobile edit') THEN TRUE
            ELSE FALSE
        END AS is_revr_mobile_edit,
        CASE
            WHEN ARRAY_CONTAINS(revision_tags, 'contenttranslation') THEN TRUE
            ELSE FALSE
        END AS is_revr_cx_edit,
        CASE
            WHEN ARRAY_CONTAINS(revision_tags, 'sectiontranslation') THEN TRUE
            ELSE FALSE
        END AS is_revr_sx_edit
    FROM
        automod_reverts amr
    INNER JOIN
        ${source_mwh_table} revr_mwh
        ON amr.wiki_db = revr_mwh.wiki_db
            AND amr.snapshot = revr_mwh.snapshot
            AND amr.amr_rev_id = revr_mwh.revision_first_identity_reverting_revision_id
    WHERE
        event_entity = 'revision'
        AND event_type = 'create'
),

-- ==============================================================================
-- data related to reverted Automoderator reverts, if applicable (prefix: revam_)
-- ==============================================================================
reverted_am_reverts AS (
    SELECT
        revr.*,
        revam_mwh.revision_id AS revam_rev_id,
        revam_mwh.event_user_text AS revam_actor_name,
        CAST(revam_mwh.event_timestamp AS TIMESTAMP) AS revam_rev_dt,
        DATE(revam_mwh.event_timestamp) AS revam_rev_date,
        CASE
            WHEN revam_mwh.event_user_is_anonymous THEN 'anonymous'
            WHEN (NOT revam_mwh.event_user_is_anonymous) AND (revam_mwh.event_user_text LIKE '~2%') THEN 'temporary'
            WHEN NOT (revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%') AND revam_mwh.event_user_revision_count < 50 THEN 'newcomer'
            ELSE 'other_registered'
        END AS revam_user_type,
        CASE
            WHEN revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%' THEN NULL
            WHEN NOT (revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%') AND revam_mwh.event_user_revision_count = 0 THEN '0'
            WHEN NOT (revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%') AND revam_mwh.event_user_revision_count BETWEEN 1 AND 5 THEN '1-5'
            WHEN NOT (revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%') AND revam_mwh.event_user_revision_count BETWEEN 6 AND 99 THEN '6-99'
            WHEN NOT (revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%') AND revam_mwh.event_user_revision_count BETWEEN 100 AND 999 THEN '100-999'
            WHEN NOT (revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%') AND revam_mwh.event_user_revision_count BETWEEN 1000 AND 4999 THEN '1000-4999'
            WHEN NOT (revam_mwh.event_user_is_anonymous OR revam_mwh.event_user_text LIKE '~2%') AND revam_mwh.event_user_revision_count >= 5000 THEN '5000+'
        END AS revam_user_editcount_bucket,      
        CASE
            WHEN revision_is_identity_reverted THEN FALSE
            WHEN revam_mwh.event_user_is_anonymous = FALSE
                 AND revam_mwh.event_user_text NOT LIKE '~2%'
                 AND revam_mwh.event_user_revision_count >= 50
            THEN TRUE
            ELSE FALSE                
        END AS is_potential_false_positive,
        revision_is_identity_reverted AS is_revam_reverted
    FROM
        reverted_edits revr
    INNER JOIN
        ${source_mwh_table} AS revam_mwh
        ON revr.wiki_db = revam_mwh.wiki_db
            AND revr.snapshot = revam_mwh.snapshot
            AND revr.amr_revam_rev_id = revam_mwh.revision_id
    WHERE
        event_entity = 'revision'
        AND event_type = 'create'
        AND is_amr_reverted
)


INSERT OVERWRITE TABLE ${destination_table}
PARTITION (snapshot = '${snapshot}')
SELECT /*+ COALESCE(${coalesce_partitions}) */
    amr.wiki_db,
    amr.is_small_wiki,
    amr.amr_rev_id,
    amr.amr_page_namespace,
    amr.amr_rev_dt,
    amr.amr_rev_date,
    amr.is_amr_revert,
    amr.is_amr_reverted,
    amr.amr_rev_parent_id,
    amr.amr_revam_rev_id,
    amr.revam_ttr_sec,
    amr.revr_page_namespace,
    amr.revr_rev_id,
    amr.revr_actor_name,
    amr.revr_rev_dt,
    amr.revr_rev_date,
    amr.revr_user_type,
    amr.revr_user_editcount_bucket,
    amr.amr_ttr_sec,
    amr.is_revr_mobile_edit,
    amr.is_revr_cx_edit,
    amr.is_revr_sx_edit,
    NULL AS revam_rev_id,
    NULL AS revam_actor_name,
    NULL AS revam_rev_dt,
    NULL AS revam_rev_date,
    NULL AS revam_user_type,
    NULL AS revam_user_editcount_bucket,
    NULL AS is_potential_false_positive,
    NULL AS is_revam_reverted
FROM
    reverted_edits amr
WHERE
    NOT amr.is_amr_reverted
UNION ALL
SELECT
    revam.wiki_db,
    revam.is_small_wiki,
    revam.amr_rev_id,
    revam.amr_page_namespace,
    revam.amr_rev_dt,
    revam.amr_rev_date,
    revam.is_amr_revert,
    revam.is_amr_reverted,
    revam.amr_rev_parent_id,
    revam.amr_revam_rev_id,
    revam.revam_ttr_sec,
    revam.revr_page_namespace,
    revam.revr_rev_id,
    revam.revr_actor_name,
    revam.revr_rev_dt,
    revam.revr_rev_date,
    revam.revr_user_type,
    revam.revr_user_editcount_bucket,
    revam.amr_ttr_sec,
    revam.is_revr_mobile_edit,
    revam.is_revr_cx_edit,
    revam.is_revr_sx_edit,
    revam.revam_rev_id,
    revam.revam_actor_name,
    revam.revam_rev_dt,
    revam.revam_rev_date,
    revam.revam_user_type,
    revam.revam_user_editcount_bucket,
    revam.is_potential_false_positive,
    revam.is_revam_reverted
FROM
    reverted_am_reverts revam
;