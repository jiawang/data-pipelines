--
-- Creates a table to store information related to small wikis for use by pipelines for Automoderator metrics.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_automoderator_small_wikis_classification.hql \
--         -d table_name=wmf_product.automoderator_small_wikis_classification \
--         -d base_directory=/wmf/data/wmf_product/automoderator/small_wikis_classification

CREATE TABLE IF NOT EXISTS ${table_name} (
    `snapshot_date`             date     COMMENT 'Date on which the classification was last updated on.',
    `wiki_db`                   string   COMMENT 'Database code of the wiki, e.g., trwiki.',
    `is_small_wiki`             boolean  COMMENT 'Indicates whether the wiki is considered a small wiki or not (within the scope of Automoderator).'
)
-- no partititions are being defined as the data will be overwritten with every run.
USING ICEBERG 
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;