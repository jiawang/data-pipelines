--
-- Creates a table for snapshot of Automoderator's activity on a monthly basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_automoderator_activity_snapshot_monthly.hql \
--         -d table_name=wmf_product.automoderator_activity_snapshot_monthly \
--         -d base_directory=/wmf/data/wmf_product/automoderator/activity_snapshot_monthly 

CREATE TABLE IF NOT EXISTS ${table_name} (
    `snapshot`                            string    COMMENT "Snapshot in MMMM-YY format",
    `wiki_db`                             string    COMMENT "MediaWiki database code, for example, trwiki",
    `is_small_wiki`                       boolean   COMMENT "Indicates whether a wiki is a small wiki (within the scope of Automoderator)",
    `amr_rev_id`                          bigint    COMMENT "Revision ID of the edit or revert made by Automoderator",
    `amr_page_namespace`                  bigint    COMMENT "Namespace of the edit or revert made by Automoderator",
    `amr_rev_dt`                          timestamp COMMENT "Timestamp of the revert made by Automoderator",
    `amr_rev_date`                        date      COMMENT "Date of the revert made by Automoderator",
    `is_amr_revert`                       boolean   COMMENT "Indicates whether the edit made by Automoderator is a revert or not",
    `is_amr_reverted`                     boolean   COMMENT "Indicates whether the revert made by Automoderator is reverted back",
    `amr_rev_parent_id`                   bigint    COMMENT "Parent revision ID of the revert made by Automoderator",
    `amr_revam_rev_id`                    bigint    COMMENT "Revision ID of edit that revereted Automoderator's revert (if applicable)",
    `revam_ttr_sec`                       bigint    COMMENT "Time (in seconds) for Automoderator's revert to be reverted back (if applicable)",
    `revr_page_namespace`                 int       COMMENT "Namespace of the edit reverted by Automoderator",
    `revr_rev_id`                         bigint    COMMENT "Revision ID of the edit reverted by Automoderator",
    `revr_actor_name`                     string    COMMENT "Actor name (includes IP or temporary account name) of the user who made the edit reverted by Automoderator",
    `revr_rev_dt`                         timestamp COMMENT "Timestamp of the edit that was reverted by Automoderator",
    `revr_rev_date`                       date      COMMENT "Date of the edit that was reverted by Automoderator",
    `revr_user_type`                      string    COMMENT "User type of the user who made the edit that was reverted by Automoderator",
    `revr_user_editcount_bucket`          string    COMMENT "Edit count bucket of the user who made the edit that was reverted by Automoderator",
    `amr_ttr_sec`                         bigint    COMMENT "Time (in seconds) for Automoderator to revert the edit",
    `is_revr_mobile_edit`                 boolean   COMMENT "Indicates whether the edit reverted was made using a mobile device",
    `is_revr_cx_edit`                     boolean   COMMENT "Indicates whether the edit reverted was made using the Content transation tool",
    `is_revr_sx_edit`                     boolean   COMMENT "Indicates whether the edit reverted was made using the Section transation tool",
    `revam_rev_id`                        bigint    COMMENT "Revision ID of the edit that reverted Automoderator's revert",
    `revam_actor_name`                    string    COMMENT "Actor name (includes IP or temporary account name) of the user who reverted Automoderator's revert",
    `revam_rev_dt`                        timestamp COMMENT "Timestamp of the edit that reverted Automoderator's revert",
    `revam_rev_date`                      date      COMMENT "Date of the edit that reverted Automoderator's revert",
    `revam_user_type`                     string    COMMENT "User type of the user who reverted Automoderator's revert",
    `revam_user_editcount_bucket`         string    COMMENT "Edit count bucket of the user who reverted Automoderator's revert",
    `is_potential_false_positive`         boolean   COMMENT "Indicates whether the Automoderator's revert can be considered as a potential false positive",
    `is_revam_reverted`                   boolean   COMMENT "Indicates wherether the revert that reverted Automoderator's revert was reverted or not"
)
USING ICEBERG
PARTITIONED BY (snapshot)
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
