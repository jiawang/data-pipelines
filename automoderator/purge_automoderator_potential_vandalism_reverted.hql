-- Purges snapshots of revert proportion handled by Automoderator, older than the three months.
--
-- Usage:
--     spark3-sql -f purge_automoderator_potential_vandalism_reverted.hql \
--         -d source_table=wmf_product.automoderator_potential_vandalism_reverted \
--         -d snapshot=2024-09

DELETE FROM ${source_table}
WHERE snapshot <= DATE_FORMAT(ADD_MONTHS('${snapshot}', -3), 'yyyy-MM')
;
