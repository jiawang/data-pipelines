--
-- Creates a table for snapshot of Automoderator's activity on a daily basis.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_automoderator_monitoring_snapshot_daily.hql \
--         -d table_name=wmf_product.automoderator_monitoring_snapshot_daily \
--         -d base_directory=/wmf/data/wmf_product/automoderator/monitoring_snapshot_daily \

CREATE TABLE IF NOT EXISTS ${table_name}(
    `snapshot_date`                date      COMMENT "Date on which the snapshot is generated on",
    `wiki_db`                      string    COMMENT "MediaWiki database code, for example, trwiki",
    `is_small_wiki`                boolean   COMMENT "Indicates whether a wiki is a small wiki (within the scope of Automoderator)",
    `amr_rev_id`                   bigint    COMMENT "Revision ID of the edit or revert made by Automoderator",
    `amr_page_namespace`           bigint    COMMENT "Namespace of the page on which the edit was made",
    `amr_rev_dt`                   timestamp COMMENT "Timestamp of the revert made by Automoderator",
    `amr_rev_date`                 date      COMMENT "Date of the revert made by Automoderator",
    `is_amr_revert`                boolean   COMMENT "Indicates whether edit is an revert or not",
    `is_amr_reverted`              boolean   COMMENT "Indicates whether the revert made by Automoderator is reverted back",
    `amr_rev_parent_id`            bigint    COMMENT "Parent revision ID of the revert made by Automoderator",
    `amr_time_to_revert_sec`       bigint    COMMENT "Time (in seconds) elapsed Automoderator reverted the edit",
    `revr_rev_id`                  bigint    COMMENT "Revision ID of the edit reverted by Automoderator (identical to amr_rev_parent_id)",
    `revr_actor_name`              string    COMMENT "Actor name (includes IP or temporary account name) of the user who made the edit reverted by Automoderator",
    `revr_rev_dt`                  timestamp COMMENT "Timestamp of the edit that was reverted by Automoderator",
    `revr_rev_date`                date      COMMENT "Date of the edit that was reverted by Automoderator",
    `revr_user_type`               string    COMMENT "User type of the user who made the edit that was reverted by Automoderator",
    `revr_user_editcount_bucket`   string    COMMENT "Edit count bucket of the user who made the edit that was reverted by Automoderator",
    `revam_rev_id`                 bigint    COMMENT "Revision ID of the edit that reverted Automoderator's revert",
    `revam_rev_dt`                 timestamp COMMENT "Timestamp of the edit that reverted Automoderator's revert",
    `revam_rev_date`               date      COMMENT "Date of the edit that reverted Automoderator's revert",
    `revam_time_to_revert_sec`     bigint    COMMENT "Time (in seconds) elapsed to revert Automoderator's revert",
    `revam_actor_name`             string    COMMENT "Actor name (includes IP or temporary account name) of the user who reverted Automoderator's revert",
    `revam_user_type`              string    COMMENT "User type of the user who reverted Automoderator's revert",
    `is_potential_false_positive`  boolean   COMMENT "Indicates whether the Automoderator's revert can be considered as a potential false positive"
)
USING ICEBERG
PARTITIONED BY (snapshot_date, wiki_db)
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
