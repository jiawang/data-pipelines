--
-- Creates a table to store configuration settings of Automoderator, for later use by other jobs/queries.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_automoderator_config.hql \
--         -d table_name=wmf_product.automoderator_config \
--         -d base_directory=/wmf/data/wmf_product/automoderator/automoderator_config

CREATE TABLE IF NOT EXISTS ${table_name} (
    `snapshot_date`             date     COMMENT 'Day on which the config is fetched.',
    `wiki_db`                   string   COMMENT 'Database code of the wiki, e.g., trwiki.',
    `automoderator_status`      boolean  COMMENT 'Status to indicate whether Automoderator is enabled or not.',
    `automoderator_user_name`   string   COMMENT 'Localized version of Automoderator’s username.'
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;