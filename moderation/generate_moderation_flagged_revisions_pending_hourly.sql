-- Parameters:
--     wiki_db: database code of the wiki for wiki the metrics are being calculated

WITH
    -- The flaggedpage_pending table only stores the revisions that are pending to be reviewed at the time of the query. 
    -- In cases where a wiki (most commonly enwiki) doesn't have any pending flagged revisions we want to have it recorded as zero. 
    -- For that, the partition columns will be selected first, and aggreated metrics will be left joined.
    query_partition AS (
        SELECT
            '{wiki_db}' AS wiki_db,
            CAST(DATE(CURRENT_TIMESTAMP) AS DATE) AS date,
            HOUR(CURRENT_TIMESTAMP) AS hour
    ),
    
    pending_frevs AS (
        SELECT
            '{wiki_db}' AS wiki_db,
            fp_page_id AS pending_fr,
            TIMESTAMPDIFF(SECOND, fp_pending_since, CURRENT_TIMESTAMP) AS elapsed_secs
        FROM
            flaggedpages
        WHERE
            fp_pending_since IS NOT NULL
    )
    
SELECT
    qp.wiki_db,
    date,
    hour,
    COALESCE(COUNT(pending_fr), 0) AS pending_revision_count,
    COALESCE(ROUND(AVG(elapsed_secs)), 0) AS mean_elapsed_secs
FROM
    query_partition qp
LEFT JOIN
    pending_frevs fr
    ON qp.wiki_db = fr.wiki_db
GROUP BY
    qp.wiki_db, date, hour
