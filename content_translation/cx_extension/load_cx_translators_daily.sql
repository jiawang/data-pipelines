-- The query will be used by a Python script (in job repo) to fetch from MariaDB-replicas,
-- and load to data lake, at wmf_raw.cx_translators, to be later used in an Airflow DAG.
SELECT
    translator_user_id,
    translator_translation_id,
    CURDATE() AS snapshot_date,
FROM
    cx_translators
;