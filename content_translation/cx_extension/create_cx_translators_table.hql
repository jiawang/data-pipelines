--
-- Creates a table to store data related to translators using the Content Translation (CX) tool.
--
-- Parameters:
--     table_name      Fully qualified name of the table to create.
--     base_directory  HDFS path to use as the table's base location.
--
-- Usage:
--     spark3-sql -f create_cx_translators.hql \
--         -d table_name=wmf_raw.cx_translators \
--         -d base_directory=/wmf/data/raw/content_translation/cx_translators

CREATE TABLE IF NOT EXISTS ${table_name} (
    `translator_user_id`        bigint  COMMENT "Key to the global user ID (gu_id) in globaluser table.",
    `translator_translation_id` bigint  COMMENT "Key to the transation_id in the cx_translations table.",
    `snapshot_date`             date    COMMENT "Date on which the data was lasted updated on."
)
USING ICEBERG
-- no partitioning is required, data will be overwritten with every run.
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${base_directory}'
;
